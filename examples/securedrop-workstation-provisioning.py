"""SecureDrop Workstation provisioning, Umami flavor"""

from umami import VM, Host, VMClass
from umami.utils import Progress, run

SYS_TEMPLATE = "fedora-37"
SDW_TEMPLATE = "securedrop-workstation-bullseye"
SDW_TEMPLATE_SMALL = "sd-small-bullseye-template"
SDW_TEMPLATE_LARGE = "sd-large-bullseye-template"
VOLUME_SIZE = "10GiB"
NETWORK_PROVIDER = "whonix-gw-16"

host = Host()
templates = host.templates()


def install_sys_template() -> Progress:
    """Install the most recent required system template"""
    if SYS_TEMPLATE not in templates.get_names():
        for progress in templates.install(SYS_TEMPLATE):
            yield progress


def update_sys_vms() -> None:
    """Update system VMs to use the most recent system template"""
    sd_fedora_dvm = host.get_vm("sd-fedora-dvm")

    if not sd_fedora_dvm:
        sd_fedora_dvm = (
            host.create_vm(VMClass.APP, "sd-fedora-dvm", "red")
            .set_netvm(None)
            .set_template(SYS_TEMPLATE)
            .set_dispvm_template(True)
        )

    sys_usb = host.get_vm("sys-usb")
    assert isinstance(sys_usb, VM)
    if sys_usb.template().name() != "sd-fedora-dvm":
        sys_usb.shutdown().set_template(sd_fedora_dvm).start()

    for name in ["sys-firewall", "sd-fedora-dvm", "sys-net"]:
        vm = host.get_vm(name)
        assert isinstance(vm, VM)
        template = vm.template().name()
        if not template.startswith(SYS_TEMPLATE):
            new = host.get_vm(SYS_TEMPLATE + "-dvm" if template.endswith("-dvm") else SYS_TEMPLATE)
            assert isinstance(new, VM)
            vm.shutdown().set_template(new).start()


def install_sdw_templates() -> Progress:
    """Install the most recent SecureDrop Workstation template"""
    template_names = list(templates.get_names())
    if SDW_TEMPLATE not in template_names:
        for progress in templates.install(SDW_TEMPLATE):
            yield progress

    for template in [SDW_TEMPLATE_SMALL, SDW_TEMPLATE_LARGE]:
        if template not in template_names:
            sdw_template = host.get_vm(SDW_TEMPLATE)
            assert isinstance(sdw_template, VM)
            sdw_template.clone(template)


def create_sd_log() -> None:
    """Create sd-log VM"""
    (
        host.create_vm(VMClass.APP, "sd-log", "red")
        .set_template(SDW_TEMPLATE_SMALL)
        .set_netvm(None)
        .set_autostart(True)
        .add_tag("sd-workstation")
        .set_feature("service.paxctld", True)
        .set_feature("service.redis", True)
        .set_feature("service.securedrop-log", True)
        .extend_volume("private", VOLUME_SIZE)
        .start()
    )


def create_sd_devices_dvm() -> None:
    """Create sd-devices-dvm Disposable Template"""
    (
        host.create_vm(VMClass.APP, "sd-devices-dvm", "red")
        .set_template(SDW_TEMPLATE_LARGE)
        .set_netvm(None)
        .add_tag("sd-workstation")
        .set_feature("service.paxctld", True)
        .set_feature("service.cups", True)
        .set_dispvm_template(True)
    )


def create_sd_devices() -> None:
    """Create sd-devices DispVM"""
    (
        host.create_vm(VMClass.DISPOSABLE, "sd-devices", "red")
        .set_template("sd-devices-dvm")
        .set_netvm(None)
        .add_tag("sd-workstation")
    )


def create_sd_app() -> None:
    """Create sd-app AppVM"""
    (
        host.create_vm(VMClass.APP, "sd-app", "yellow")
        .set_template(SDW_TEMPLATE_SMALL)
        .set_netvm(None)
        .add_tags(["sd-client", "sd-workstation"])
        .set_feature("service.paxctld", True)
        .extend_volume("private", VOLUME_SIZE)
        .start()
    )


def create_sd_gpg() -> None:
    """Create sd-gpg AppVM"""
    (
        host.create_vm(VMClass.APP, "sd-gpg", "purple")
        .set_template(SDW_TEMPLATE_SMALL)
        .set_netvm(None)
        .add_tag("sd-workstation")
        .set_autostart(True)
        .start()
    )


def create_sd_whonix() -> None:
    """Create sd-whonix AppVM"""
    (
        host.create_vm(VMClass.APP, "sd-whonix", "purple")
        .set_template(NETWORK_PROVIDER)
        .set_netvm("sys-firewall")
        .set_memory("500MB", None)
        .add_tag("sd-workstation")
        .set_autostart(True)
        .set_provides_network(True)
        .set_kernelopts("nopat apparmor=1 security=apparmor")
    )


def create_sd_proxy() -> None:
    """Create sd-proxy AppVM"""
    (
        host.create_vm(VMClass.APP, "sd-proxy", "blue")
        .set_template(SDW_TEMPLATE_SMALL)
        .set_netvm("sd-whonix")
        .add_tag("sd-workstation")
        .set_autostart(True)
        .start()
    )


def create_sd_viewer() -> None:
    """Create sd-viewer DispVM template"""
    sd_viewer = (
        host.create_vm(VMClass.APP, "sd-viewer", "green")
        .set_template(SDW_TEMPLATE_LARGE)
        .set_netvm(None)
        .add_tags(["sd-viewer-vm", "sd-workstation"])
        .set_feature("service.paxctld", True)
        .set_dispvm(None)
        .set_dispvm_template(True)
    )
    host.set_default_dispvm(sd_viewer)


def main() -> None:
    steps = [
        (install_sys_template, f"Install system template ({SYS_TEMPLATE})"),
        (update_sys_vms, str(update_sys_vms.__doc__)),
        (install_sdw_templates, f"Install SecureDrop Workstation template ({SDW_TEMPLATE})"),
        (create_sd_log, str(create_sd_log.__doc__)),
        (create_sd_devices_dvm, str(create_sd_devices_dvm.__doc__)),
        (create_sd_devices, str(create_sd_devices.__doc__)),
        (create_sd_app, str(create_sd_app.__doc__)),
        (create_sd_gpg, str(create_sd_gpg.__doc__)),
        (create_sd_whonix, str(create_sd_whonix.__doc__)),
        (create_sd_proxy, str(create_sd_proxy.__doc__)),
        (create_sd_viewer, str(create_sd_viewer.__doc__)),
    ]

    for state, level, total, current, desc in run(steps):
        indent = "    " * level
        current += 1
        print(f"{indent}{state}: {desc} ({current} of {total})")


if __name__ == "__main__":
    main()
