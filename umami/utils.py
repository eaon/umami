from enum import Enum
from typing import Callable, Generator, List, Tuple, Union


class RunState(Enum):
    STARTED = 1
    RUNNING = 2
    FINISHED = 3

    def __str__(self) -> str:
        if self == RunState.STARTED:
            return "Started"
        elif self == RunState.RUNNING:
            return "Running"
        assert self == RunState.FINISHED
        return "Finished"


Total = int
Current = int
Level = int
Progress = Generator[Tuple[RunState, Level, Total, Current, str], None, None]


def run(steps: List[Tuple[Callable[[], Union[None, Progress]], str]]) -> Progress:
    """Provisions steps and yields progress status"""
    total = len(steps)
    for i, (step, desc) in enumerate(steps):
        yield (RunState.STARTED, 0, total, i, desc)
        substeps = step()
        if substeps:
            yield (RunState.RUNNING, 0, total, i, desc)
            for substep in substeps:
                if callable(substep):
                    yield substep()
        yield (RunState.FINISHED, 0, total, i, desc)
