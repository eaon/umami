"""Umami supports complex linear imperative VM provisioning for QubesOS"""

from __future__ import annotations

from collections.abc import KeysView
from enum import Enum
from time import sleep
from typing import Dict, Generator, List, Optional, Tuple, Union

import qubesadmin
from qubesadmin.utils import parse_size

from umami import utils


class VirtMode(Enum):
    """Virtualization Mode for Xen Domains"""

    PVH = "pvh"
    HVM = "hvm"
    PV = "pv"


class PowerState(Enum):
    """Possible power states for VMs"""

    HALTED = "Halted"
    TRANSIENT = "Transient"
    RUNNING = "Running"
    PAUSED = "Paused"
    SUSPENDED = "Suspended"
    HALTING = "Halting"
    DYING = "Dying"
    CRASHED = "Crashed"
    NOTAVAILABLE = "NA"


class VMClass(Enum):
    """QubesOS' Virtual Machine Classes"""

    APP = "AppVM"
    STANDALONE = "StandaloneVM"
    TEMPLATE = "TemplateVM"
    DISPOSABLE = "DispVM"


class VM:
    """Virtual Machine abstraction layer"""

    def __init__(self, vm, host: Host) -> None:
        self.vm = vm
        self.host = host

    def name(self) -> str:
        """Name for the VM"""
        return self.vm.name

    def start(self) -> VM:
        """Start this VM"""
        self.vm.start()
        return self

    def pause(self) -> VM:
        self.vm.pause()
        return self

    def unpause(self) -> VM:
        self.vm.unpause()
        return self

    def shutdown(self, recursive: bool = True) -> VM:
        queue = [self]
        if recursive:
            queue.extend(self.dependents(recursive=True))
            queue.reverse()

        for vm in queue:
            if vm.power_state() in [PowerState.RUNNING, PowerState.SUSPENDED, PowerState.PAUSED]:
                vm.vm.shutdown()
                while vm.power_state() != PowerState.HALTED:
                    sleep(0.1)

        return self

    def kill(self) -> VM:
        self.vm.kill()
        while self.vm.power_state() is not PowerState.HALTED:
            sleep(0.2)
        return self

    def clone(self, name: str) -> VM:
        """Clone VM and return the clone"""
        self.host.qubes.clone_vm(self.vm, name)
        return VM(self.host.qubes.domains[name], self.host)

    def remove(self) -> None:
        del self.vm
        del self

    def tags(self) -> List[str]:
        return list(self.vm.tags)

    def add_tag(self, tag: str) -> VM:
        return self.add_tags([tag])

    def add_tags(self, tags: List[str]) -> VM:
        for tag in tags:
            self.vm.tags.add(tag)
        return self

    def remove_tag(self, tag: str) -> VM:
        self.vm.tags.remove(tag)
        return self

    def set_autostart(self, autostart: bool) -> VM:
        self.vm.autostart = autostart
        return self

    def autostart(self) -> bool:
        return self.vm.autostart

    def set_memory(self, minmem: Union[str, int, None], maxmem: Union[str, int, None]) -> VM:
        if minmem:
            self.vm.memory = int((parse_size(minmem) if isinstance(minmem, str) else minmem) / 1e6)
        if maxmem:
            self.vm.maxmem = int((parse_size(maxmem) if isinstance(maxmem, str) else maxmem) / 1e6)
        return self

    def memory(self) -> Tuple[int, int]:
        return (int(self.vm.memory * 1e6), int(self.vm.maxmem * 1e6))

    def set_netvm(self, vm: Union[VM, str, None]) -> VM:
        if isinstance(vm, VM):
            self.vm.netvm = vm.vm
        elif isinstance(vm, str):
            self.vm.netvm = self.host.get_vm(vm, qubesvm=True)
        else:
            self.vm.netvm = None
        return self

    def netvm(self) -> Optional[VM]:
        """Returns a VM object rather than just the name"""
        if self.vm.netvm:
            return VM(self.vm.netvm, self.host)
        return None

    def set_label(self, label: str) -> VM:
        self.vm.label = self.host.qubes.labels[label]
        return self

    def label(self) -> str:
        return str(self.vm.label)

    def set_virt_mode(self, mode: VirtMode) -> VM:
        self.vm.virt_mode = mode.value
        return self

    def virt_mode(self) -> VirtMode:
        return VirtMode(self.vm.virt_mode)

    def set_vcpus(self, vcpus: int) -> VM:
        self.vm.vcpus = vcpus
        return self

    def vcpus(self) -> int:
        return self.vm.vcpus

    def set_timeout(self, timeout: int) -> VM:
        self.vm.qrexec_timeout = timeout
        return self

    def set_feature(self, feature: str, value: Union[bool, str]) -> VM:
        self.vm.features[feature] = value if isinstance(value, str) else str(int(value))
        return self

    def unset_feature(self, feature: str) -> VM:
        del self.vm.features[feature]
        return self

    def feature(self, feature: str) -> str:
        return self.vm.features[feature]

    def features(self) -> Dict[str, str]:
        return {feature: self.vm.features[feature] for feature in self.vm.features}

    def extend_volume(self, volume: str, size: str) -> VM:
        size = parse_size(size)
        if self.vm.volumes[volume].size < size:
            self.vm.volumes[volume].resize(size)
        return self

    def disk_utilization(self) -> int:
        return self.vm.get_disk_utilization()

    def current_memory(self) -> int:
        return self.vm.get_mem()

    def power_state(self) -> PowerState:
        return PowerState(self.vm.get_power_state())

    def run(self, command: str, stdin: Optional[bytes]) -> Tuple[bytes, bytes]:
        return self.vm.run(command, stdin)

    def appvms(self) -> Optional[List[VM]]:
        appvms = list(self.vm.appvms)
        if appvms:
            return [VM(vm, self.host) for vm in appvms]
        return None

    def connected_vms(self) -> Optional[List[VM]]:
        connected_vms = list(self.vm.connected_vms)
        if connected_vms:
            return [VM(vm, self.host) for vm in connected_vms]
        return None

    def qube_class(self) -> VMClass:
        return VMClass(self.vm.klass)

    def set_template(self, vm: Union[VM, str]) -> VM:
        if isinstance(vm, VM):
            self.vm.template = vm.vm
        else:
            self.vm.template = self.host.get_vm(vm, qubesvm=True)
        return self

    def template(self) -> VM:
        return VM(self.host.qubes.domains[self.vm.template.name], self.host)

    def set_dispvm(self, vm: Union[VM, str, None]) -> VM:
        if isinstance(vm, VM):
            self.vm.default_dispvm = vm
        elif isinstance(vm, str):
            self.vm.default_dispvm = self.host.get_vm(vm, qubesvm=True)
        else:
            self.vm.default_dispvm = None
        return self

    def dispvm(self) -> VM:
        return VM(self.vm.default_dispvm, self.host)

    def set_dispvm_template(self, dispvm_template: bool) -> VM:
        self.vm.template_for_dispvms = dispvm_template
        return self

    def dispvm_template(self) -> bool:
        return self.vm.template_for_dispvms

    def set_guivm(self, vm: Union[VM, str]) -> VM:
        self.vm.guivm = vm.vm if isinstance(vm, VM) else self.host.get_vm(vm, qubesvm=True)
        return self

    def guivm(self) -> VM:
        return VM(self.vm.guivm, self.host)

    def set_audiovm(self, vm: Union[VM, str]) -> VM:
        self.vm.audiovm = vm.vm if isinstance(vm, VM) else self.host.get_vm(vm, qubesvm=True)
        return self

    def audiovm(self) -> VM:
        return VM(self.vm.audiovm, self.host)

    def set_kernel(self, kernel: str) -> VM:
        self.vm.kernel = kernel
        return self

    def kernel(self) -> str:
        return self.vm.kernel

    def set_kernelopts(self, opts: str) -> VM:
        self.vm.kernelopts = opts
        return self

    def kernelopts(self) -> str:
        return self.vm.kernelopts

    def set_debug(self, debug: bool) -> VM:
        self.vm.debug = debug
        return self

    def debug(self) -> bool:
        return self.vm.debug

    def set_provides_network(self, provides_network: bool) -> VM:
        self.vm.provides_network = provides_network
        return self

    def provides_network(self) -> bool:
        return self.vm.provides_network

    def dependents(self, recursive: bool = False) -> List[VM]:
        def get_dependents(vm: VM, prerequisite: VM) -> Generator[VM, None, None]:
            for attr in ["netvm", "guivm", "audiovm"]:
                if vm not in dependents and getattr(vm.vm, attr, None) == prerequisite.vm:
                    yield vm

        dependents = []
        queue = [self]
        while queue:
            local_dependents = []
            for prerequisite in queue:
                for vm in self.host.get_vms():
                    for dependent in get_dependents(vm, prerequisite):
                        if dependent not in dependents:
                            local_dependents.append(dependent)

            queue = local_dependents if recursive else []

            if local_dependents:
                dependents.extend(local_dependents)

        return dependents


class Templates:
    def __init__(self, host: Host) -> None:
        self.host = host

    def get(self) -> Generator[VM, None, None]:
        return (VM(vm, self.host) for vm in self.host.qubes.domains if not hasattr(vm, "template"))

    def get_names(self) -> Generator[str, None, None]:
        return (vm.name() for vm in self.get())

    def install(self, name: str) -> utils.Progress:
        # Shell out?
        return VM(self.host.qubes.domains[name], self.host)


class Host:
    def __init__(self, qubes: qubesadmin.app.QubesBase = qubesadmin.Qubes()) -> None:
        self.qubes = qubes

    def get_vm(self, name: str, qubesvm=False) -> Union[VM, qubesadmin.vm.QubesVM, None]:
        try:
            domain = self.qubes.domains[name]
            if qubesvm:
                return domain
            return VM(domain, self)
        except Exception:
            return None

    def get_vms(self) -> Generator[VM, None, None]:
        return (VM(vm, self) for vm in self.qubes.domains)

    def create_vm(self, vmclass: VMClass, name: str, label: str) -> VM:
        self.qubes.add_new_vm(vmclass.value, name=name, label=self.qubes.labels[label])
        return VM(self.qubes.domains[name], self)

    def vm_names(self) -> KeysView:
        return self.qubes.domains.keys()

    def templates(self) -> Templates:
        return Templates(self)

    def set_default_dispvm(self, vm: Union[VM, str]) -> None:
        self.qubes.default_dispvm = vm.vm if isinstance(vm, VM) else self.get_vm(vm, qubesvm=True)
